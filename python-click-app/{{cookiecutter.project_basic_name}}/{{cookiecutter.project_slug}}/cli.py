from pathlib import Path
from typing import Optional
from logging import DEBUG, INFO

import click
from yaml import safe_load
from logzero import loglevel as set_loglevel

from {{ cookiecutter.project_slug }}.config import AppConfig
from {{ cookiecutter.project_slug }}.log import log

DEFAULT_CONFIG = Path.cwd() / 'config.yaml'


@click.command()
@click.option('-c', '--config-file', default=None,
              type=click.Path(exists=True, file_okay=True, dir_okay=False,
                              writable=False, readable=True,
                              resolve_path=True, allow_dash=False),
              help="Path to the configuration file, defaults to ./config.yaml")
@click.option('--debug', is_flag=True, help='Set logging level to DEBUG')
def main(config_file: Optional[str], debug: bool) -> None:
    """A script for printing hello world based on some configured values"""
    set_loglevel(DEBUG if debug else INFO)
    config_path = DEFAULT_CONFIG if config_file is None else Path(config_file)
    config = AppConfig(**safe_load(config_path.open()))
    name = config.user_name if config.user_name else 'world'
    suffix = config.hello_suffix if config.hello_suffix else ''
    print(f"Hello, {name}{suffix}")
    log.debug('Done!')
