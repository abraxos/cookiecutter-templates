from typeguard import typechecked

@typechecked
def hello(target: str, suffix: str):
    return f'Hello, {target}{suffix}'
