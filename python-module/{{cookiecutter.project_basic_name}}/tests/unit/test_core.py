from typing import Any

from pytest import mark, raises

from {{ cookiecutter.project_slug }}.core import hello

TESTDATA = [
    ("Eugene", "!", "Hello, Eugene!", None),
    ("Eugene", "?", "Hello, Eugene?", None),
    (None, "?", None, TypeError),
]


@mark.parametrize("name, suffix, expected, exc", TESTDATA)
def test_hello(name: Any, suffix: Any, expected: str,
               exc: BaseException) -> None:
    if exc is not None:
        with raises(exc):
            assert hello(name, suffix) == expected
    else:
        assert hello(name, suffix) == expected
