# {{ cookiecutter.project_title }}

{{ cookiecutter.project_summary }}

## Installation & Setup

_TODO_

### Configuration

_TODO_

## Development

_TODO_

### Testing

_TODO_

#### Virtual Testing Database

This project comes with a virtual [PostgreSQL](https://www.postgresql.org/) database server for testing managed with [Vagrant](https://www.vagrantup.com/) configured using [Ansible](https://www.ansible.com/). You can use the following commands to set it up and interact with it (all from the directory with `Vagrantfile` in it:

1. To set up and provision the VM (can be done repeatedly to reprovision): `vagrant up --provision`
2. To restart the VM (if you changed the Vagrant configuration or messed it up): `vagrant reload --provision`
3. To destroy the VM: `vagrant destroy -f`
4. To SSH into the VM: `vagrant ssh`
5. Within the VM you can connect to the database as the `vagrant` user with: `psql -d {{ cookiecutter.db_name }}`

The database server is configured with automatic access for the `vagrant` user entire project directory should be synced to the `/host` directory on the VM and you can test anything from the project directory from inside the VM.

The database structure is controlled by the `roles/postgres/files/upgrade.d/` and `roles/postgres/files/downgrade.d/`. All SQL files inside should be named: `<target-version>_<description_slug>.sql`. So, for example  to upgrade the database you need to make a file called `12_added_new_user_schema.sql` and a corresponding `11_delete_user_schema.sql` in the `downgrade.d/` directory. During a provisioning of the VM, all downgrade files are executed in reverse order, and then all upgrade files are executed in order.