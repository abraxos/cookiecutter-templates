# Vagrant/Ansible Learning Template

This template is designed to quickly set up a virtualized Ubuntu environment for learning different technologies.

## Setup

Note that this template uses libvirt/qemu for virtualization and *not* VirtualBox, which means that you will need to install the following to use it:

```bash
sudo apt-get install vagrant libvirt-bin libvirt-dev qemu-utils qemu # apt packages
sudo vagrant plugin install vagrant-libvirt # vagrant plugin for libvirt
```
