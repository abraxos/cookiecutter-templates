from pathlib import Path
from re import compile as regex
from subprocess import check_output, STDOUT

V_PAT = regex(r'[0-9a-zA-Z_\.]+\s\((?P<version>[\d\.]+)\)')

def latest_version(package: str) -> str:
    pip_output = check_output(['pip3', 'index', 'versions', package], stderr=STDOUT).decode()
    return V_PAT.findall(pip_output)[0]

PYPROJECT_TOML = f'''
[tool.poetry]
name = "{{ cookiecutter.project_basic_name }}"
version = "0.1.0"
description = "{{ cookiecutter.project_summary }}"
authors = ["{{ cookiecutter.dev_name }} <{{ cookiecutter.dev_email }}>"]
classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
]
documentation="{{ cookiecutter.project_url }}"
repository="{{ cookiecutter.project_url }}"
readme="README.md"
license="GPL-3.0-only"

[tool.poetry.dependencies]
python = "^3.7"
arrow = "^{latest_version('arrow')}"
click = "^{latest_version('click')}"
click-path = "^{latest_version('click-path')}"
funcy = "^{latest_version('funcy')}"
gunicorn = "^{latest_version('gunicorn')}"
logzero = "^{latest_version('logzero')}"
pydantic = "^{latest_version('pydantic')}"
pyramid = "^{latest_version('pyramid')}"
pyramid_openapi3 = "^{latest_version('pyramid_openapi3')}"
pyyaml = "^{latest_version('pyyaml')}"
typeguard = "^{latest_version('typeguard')}"

[tool.poetry.dev-dependencies]
flake8 = "^{latest_version('flake8')}"
mypy = "^{latest_version('mypy')}"
pytest = "^{latest_version('pytest')}"
pytest_cov = "^{latest_version('pytest-cov')}"
pylint = "^{latest_version('pylint')}"
requests = "^{latest_version('requests')}"
webtest = "^{latest_version('webtest')}"

[tool.poetry.scripts]
{{ cookiecutter.project_basic_name }}-server = '{{ cookiecutter.project_slug }}.cli:main'

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
'''

def generate_pyproject_toml() -> None:
    pyproject_file = Path.cwd() / 'pyproject.toml'
    pyproject_file.open('w+').write(PYPROJECT_TOML)

generate_pyproject_toml()