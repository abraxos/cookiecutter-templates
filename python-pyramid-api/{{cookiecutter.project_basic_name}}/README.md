# {{ cookiecutter.project_title }}

{{ cookiecutter.project_summary}}

## Setup & Usage

### Installation

### Setup

### Usage

_Note: please check out the **First Run** section below as well._

TODO

### Configuration

TODO

## Development

Using poetry install from inside the repo directory:

```
poetry install
```

This will set up a virtualenv which you can always activate with either `poetry shell` or run specific commands with `poetry run...`. All instructions below that are meant to be run in the virtualenv will be prefaced with `({{ cookiecutter.project_basic_name }})$` 

### Standards

- Be excellent to each other
- Code coverage must be at 100% for all new code, or a good reason must be provided for why a given bit of code is not covered.
  - Example of an acceptable reason: "There is a bug in the code coverage tool and it says its missing this, but its not".
  - Example of unacceptable reason: "This is just exception handling, its too annoying to cover it".
- The code must pass the following analytics tools. Similar exceptions are allowable as mentioned above.
  - `pylint --disable=C0103,C0111,W1203,R0903,R0913 --max-line-length=120 {{ cookiecutter.project_slug }}`
  - `flake8 --max-line-length=120 {{ cookiecutter.project_slug }}`
  - `mypy --ignore-missing-imports --follow-imports=skip --strict-optional {{ cookiecutter.project_slug }}`
- All incoming information from clients and configurations should be validated.
- All internal arguments passing should be typechecked whenever possible with `typeguard.typechecked`

### IDE Setup

#### Sublime Text 3

```
curl -sSL https://gitlab.com/-/snippets/2066312/raw/master/poetry.sublime-project.py | poetry run python
```

TODO: Add more IDE setups

### First Run

You can then run the server with the example configuration like so:

```bash
({{ cookiecutter.project_basic_name }})$ {{ cookiecutter.project_basic_name }}-server --config tests/examples/example_config.yaml --debug
```

Assuming you did not configure a different port, you should then be able to access the healthcheck endpoint with: `curl -X GET "http://localhost:5000/healthcheck" -H  "accept: application/json"`. Similarly, you can see the OpenAPI specification for the server at http://localhost:5000/docs/ (this is not available unless you pass in the `--debug` parameter).

While developing, you can use [`watchexec`](https://github.com/watchexec/watchexec) to monitor the file system for changes and relaunch the server:

```bash
({{ cookiecutter.project_basic_name }})$ watchexec -r -e py,yaml "{{ cookiecutter.project_basic_name }}-server --debug -c tests/examples/example_config.yaml"
```

### Testing

All testing should be done with `pytest` which is installed with the `dev` requirements.

To run all the unit tests, execute the following from the repo directory:

```bash
({{ cookiecutter.project_basic_name }})$ pytest
```

This should produce a coverage report in `/path/to/{{ cookiecutter.project_basic_name }}/htmlcov/`

To run a specific test file:

```bash
({{ cookiecutter.project_basic_name }})$ pytest tests/unit/test_types.py
```

To run a specific test:

```bash
({{ cookiecutter.project_basic_name }})$ pytest tests/unit/test_metrics_manager.py::test_multi_proc_metrics_multiple_registration
```

For more information on testing, see the `pytest.ini` file as well as the [documentation](https://docs.pytest.org/en/stable/).
