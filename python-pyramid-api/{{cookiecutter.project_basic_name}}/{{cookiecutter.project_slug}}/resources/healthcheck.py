from os import access, R_OK, W_OK
from typing import Optional

from pyramid.request import Request
from pyramid.exceptions import HTTPForbidden
from typeguard import typechecked

from {{ cookiecutter.project_slug }}.utils.config import AppConfig
from {{ cookiecutter.project_slug }}.utils.log import log
from {{ cookiecutter.project_slug }}.utils.types import Health
from {{ cookiecutter.project_slug }}.utils.validation import params
from {{ cookiecutter.project_slug }}.metrics import MultiprocessMetrics
from {{ cookiecutter.project_slug }}.resources.json_resource import JsonResource


class Healthcheck(JsonResource):
    route = 'healthcheck'

    @typechecked
    def __init__(self, config: AppConfig, metrics: MultiprocessMetrics):
        self.config = config
        self.metrics = metrics

    @typechecked
    def _route(self) -> str:
        return 'healthcheck'

    @typechecked
    def _get_health_metrics(self, minutes_ago: Optional[int] = None) -> Health:
        seconds_ago = 60 * (minutes_ago or 1)
        if seconds_ago > self.metrics.ttl_seconds:
            raise HTTPForbidden(f"Cannot provide metrics for more than {self.metrics.ttl_seconds // 60} minutes ago")
        return Health(self.metrics.status_code_summary(seconds_ago))

    @params(query_args={'minutes_ago'})
    def get(self, request: Request, minutes_ago: Optional[int]) -> Health:  # pylint: disable=W0221
        health = self._get_health_metrics(minutes_ago)
        log.debug(f"Healthcheck: {health}")
        return health
