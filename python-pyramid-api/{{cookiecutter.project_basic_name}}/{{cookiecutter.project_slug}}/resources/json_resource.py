from abc import ABC

from pyramid.config import Configurator
from pyramid.httpexceptions import HTTPForbidden
from pyramid.request import Request
from pyramid.response import Response
from typeguard import typechecked


class JsonResource(ABC):
    # TODO: Add UUID logging functionality for each request
    route: str

    @typechecked
    def has_http_method(self, method_name: str) -> bool:
        return callable(getattr(self, method_name, None))

    @typechecked
    def register(self, configurator: Configurator):
        configurator.add_route(self.route, '/' + self._route())

        def _get(request: Request) -> Response:
            return self.get(request)

        def _head(request: Request) -> Response:
            return self.head(request)

        def _post(request: Request) -> Response:
            return self.post(request)

        def _put(request: Request) -> Response:
            return self.put(request)

        def _delete(request: Request) -> Response:
            return self.delete(request)

        def _connect(request: Request) -> Response:
            return self.connect(request)

        def _options(request: Request) -> Response:
            return self.options(request)

        def _trace(request: Request) -> Response:
            return self.trace(request)

        configurator.add_view(self.get,
                              request_method='GET',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_head,
                              request_method='HEAD',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_post,
                              request_method='POST',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_put,
                              request_method='PUT',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_delete,
                              request_method='DELETE',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_connect,
                              request_method='CONNECT',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_options,
                              request_method='OPTIONS',
                              route_name=self.route,
                              renderer='json')
        configurator.add_view(_trace,
                              request_method='TRACE',
                              route_name=self.route,
                              renderer='json')
        return self

    def _route(self) -> str:
        return self.route

    @typechecked
    def get(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def head(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def post(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def put(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def delete(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def connect(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def options(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover

    @typechecked
    def trace(self, request: Request) -> Response:  # pylint: disable=R0201
        raise HTTPForbidden  # pragma: no cover
