from pathlib import Path
from contextlib import contextmanager
from tempfile import TemporaryDirectory
from typing import Optional
import os
import sys

from typeguard import typechecked
import yaml

from {{ cookiecutter.project_slug }}.utils.log import log
from {{ cookiecutter.project_slug }}.utils.config import AppConfig


@contextmanager
def pushd(new_dir: Path):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(previous_dir)


@typechecked
def load_config(config_path: Path) -> Optional[AppConfig]:
    try:
        with pushd(config_path.parent):  # This makes paths relative to the config work
            return AppConfig(**yaml.safe_load(config_path.open('r')))
    except Exception as exc:  # pylint: disable=W0703
        log.critical(f"Cannot load config from {config_path}: {exc}")
        return None


@typechecked
def load_configuration_or_die(
        config_path: Optional[Path] = None) -> AppConfig:
    """Loads configuration and validates it or exits the application."""
    log.info(f"Loading configuration from: {config_path}")
    if config_path is not None and config_path.exists():
        config = load_config(config_path)
        if config is not None:
            log.debug(f"Configuration loaded successfully: {config_path}")
            return config
        sys.exit(2)
    log.critical("Unable to load configuration from any of the provided paths")
    sys.exit(2)


@contextmanager
def temp_dir():
    with TemporaryDirectory() as dir_name:
        yield Path(dir_name)
