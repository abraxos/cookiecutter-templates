from typing import Union
from multiprocessing import cpu_count
from os import access, R_OK, W_OK
from pathlib import Path

from typeguard import typechecked
from pydantic.dataclasses import dataclass
from pydantic import validator

from {{ cookiecutter.project_slug }}.utils.types import Strict


@dataclass(config=Strict)
class AppConfig:
    bind: str = '127.0.0.1'
    port: int = 5000

    @validator('port', pre=True)
    @typechecked
    def valid_port(cls, val: int) -> int:  # pylint: disable=R0201,E0213
        if 0 < val <= 65535:
            return val
        raise ValueError(f"Not a valid port number: {val}")

