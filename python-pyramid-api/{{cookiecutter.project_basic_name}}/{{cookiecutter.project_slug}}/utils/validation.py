from enum import Enum
from functools import wraps
from inspect import isclass, getfullargspec
from typing import _SpecialForm, _GenericAlias, get_type_hints, get_args, get_origin, Any, Union, Dict, Iterable
from typing import Callable, Optional, Type, Set

from funcy import first
from pyramid.request import Request
from pyramid.exceptions import HTTPBadRequest
from typeguard import typechecked

from {{ cookiecutter.project_slug }}.utils.log import log


@typechecked
def _is_optional(arg_type: Any) -> bool:
    return get_origin(arg_type) is Union and type(None) in get_args(arg_type)


@typechecked
def _is_int(candidate: Union[str, int, float]) -> bool:
    try:
        int(candidate)
        return True
    except ValueError:
        return False


@typechecked
def _is_float(candidate: Union[str, int, float]) -> bool:
    try:
        float(candidate)
        return True
    except ValueError:
        return False


@typechecked
def _is_enum(candidate_type: Union[Type, _GenericAlias, _SpecialForm]) -> bool:
    return isclass(candidate_type) and issubclass(candidate_type, Enum)


@typechecked
def _is_optional_enum(candidate_type: Union[Type, _GenericAlias, _SpecialForm]) -> bool:
    return any(_is_enum(a) for a in get_args(candidate_type))


@typechecked
def _enum_type(candidate_type: Union[Type, _GenericAlias, _SpecialForm]) -> Optional[Type[Enum]]:
    if _is_enum(candidate_type):
        return candidate_type
    if _is_optional_enum(candidate_type):
        return first(a for a in get_args(candidate_type) if _is_enum(a))
    return None


@typechecked
def _func_defaults(func: Callable) -> Dict[str, Any]:
    return {k: v for k, v in zip(reversed(getfullargspec(func).args or []),
                                 reversed(getfullargspec(func).defaults or []))}


@typechecked
def _has_default(func: Callable, arg: str) -> bool:
    log.debug(_func_defaults(func))
    return arg in _func_defaults(func)


@typechecked
def _process_args(self: Any,
                  func: Callable,
                  arg_names: Iterable[str],
                  request: Request,
                  **kwargs) -> Any:
    '''
    Used by decorators which interpret type-hinted parameters from the function definition and
    expects them to be in the given parameters dict. If they are not present, or do not match
    the provided type information, then this decorator will return a 400 error.

    Currently explicitly supports int, float, str, Any, and Optional(s) of the same

    TODO: Support enums
    TODO: Support Unions
    '''
    parameters = _params_dict(request)
    for arg_name, arg_type in filter(lambda i: i[0] in arg_names, get_type_hints(func).items()):
        if arg_name not in parameters:
            if _is_optional(arg_type):
                kwargs[arg_name] = None
            elif not _has_default(func, arg_name):
                raise HTTPBadRequest(f"Missing required argument: {arg_name}")
        else:  # param has been supplied
            arg = parameters[arg_name]
            if arg_type is Any:
                kwargs[arg_name] = arg
            elif (arg_type is int or int in get_args(arg_type)) and _is_int(arg):
                kwargs[arg_name] = int(arg)
            elif (arg_type is float or float in get_args(arg_type)) and _is_float(arg):
                kwargs[arg_name] = float(arg)
            elif my_enum := _enum_type(arg_type):
                if arg.upper() in my_enum.__members__:  # note that all enums have to have ALL CAPS members
                    kwargs[arg_name] = my_enum.__members__[arg.upper()]
                else:
                    raise HTTPBadRequest(f"Invalid param value: {arg}, must be one of: "
                                         f"{', '.join([m.lower() for m, _ in my_enum.__members__.items()])}")
            elif (arg_type is str or str in get_args(arg_type)):
                # all of these are encoded as strings anyway
                kwargs[arg_name] = arg
            else:
                raise HTTPBadRequest(f"Incorrect param type {arg_name}: {arg}")
    # if all type-checks pass, return the function result with kwargs generated from request
    return func(self, request, **kwargs)


@typechecked
def _params_dict(request: Request) -> Dict[str, Any]:
    return dict(request.matchdict, **{k: request.params[k] for k in request.params})


@typechecked
def params(query_args: Optional[Set[str]] = None,
           path_args: Optional[Set[str]] = None):
    '''
    A decorator for processing query parameters with validation and type-safety
    '''
    query_args = query_args or set()
    path_args = path_args or set()

    def decorator_query_param(func):
        arg_names = query_args | path_args

        @wraps(func)
        def wrapper_query_param(self, request: Request, **kwargs):
            return _process_args(self, func, arg_names, request, **kwargs)
        return wrapper_query_param
    return decorator_query_param
