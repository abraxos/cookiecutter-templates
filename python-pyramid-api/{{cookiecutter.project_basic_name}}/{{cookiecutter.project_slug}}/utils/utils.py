import multiprocessing

from typeguard import typechecked
import arrow

from {{ cookiecutter.project_slug }}.utils.types import Timestamp


def number_of_workers():
    return multiprocessing.cpu_count() + 1


@typechecked
def now() -> Timestamp:
    return Timestamp(arrow.now().int_timestamp)
