from collections import defaultdict
from logging import DEBUG, INFO  # noqa: F401 # pylint: disable=W0611
from uuid import uuid4


from logzero import loglevel as set_loglevel  # noqa: F401 # pylint: disable=W0611
from logzero import logger as log  # noqa: F401 # pylint: disable=W0611
from logzero import LogFormatter, formatter as set_formatter
from pyramid.request import Request
from typeguard import typechecked

DEFAULT_FMT = '%(color)s[%(asctime)s] [%(process)d] [%(levelname)s] [%(threadName)s]%(end_color)s %(message)s'
DEBUG_FMT = '%(color)s[%(asctime)s] [%(process)d] [%(levelname)s] [%(threadName)s] [%(processName)s:%(module)s:%(funcName)s:%(lineno)d]%(end_color)s %(message)s'


@typechecked
def uuid(_: Request) -> str:
    '''
    This function is used by the Configurator to append a unique identifier to each request
    that comes in. This way, when logging inside a resource we can prepend the information
    to every log statement associated with processing a given request. This is useful for
    tracing problems in logs in a multi-process, high throughput environment.
    '''
    return str(uuid4())


class DefaultLogFormatter(LogFormatter):
    def __init__(self, formatters,
                 default_format=DEFAULT_FMT,
                 date_format='%Y-%m-%d %H:%M:%S %z'):
        self.formats = defaultdict(lambda: default_format)
        for key, value in formatters.items():
            self.formats[key] = value
        LogFormatter.__init__(self, fmt=default_format, datefmt=date_format)

    def format(self, record):
        self._fmt = self.formats[record.levelno]
        return LogFormatter.format(self, record)


set_formatter(DefaultLogFormatter({DEBUG: DEBUG_FMT},
                                  default_format=DEFAULT_FMT))
