from typing import Type, Any, Dict

import pkg_resources
from pydantic.dataclasses import dataclass
from pyramid.request import Request
from typeguard import typechecked


VERSION = pkg_resources.require("{{ cookiecutter.project_basic_name }}")[0].version


class Strict:
    extra = "forbid"


class Timestamp(int):
    @typechecked
    def __new__(cls: Type['Timestamp'], value: int, *_: Any, **__: Any):
        if value < 0:
            raise ValueError(f"Invalid time stamp: {value}")
        return super(Timestamp, cls).__new__(cls, value)


class StatusCode(int):
    @typechecked
    def __new__(cls: Type['StatusCode'], value: int, *_: Any, **__: Any):
        if value < 0 or value > 600:
            raise ValueError(f"Invalid status code: {value}")
        return super(StatusCode, cls).__new__(cls, value)


@dataclass(config=Strict)
class Health():
    status_codes: Dict[StatusCode, int]
    version: str = VERSION

    @typechecked
    def __json__(self, _: Request):
        return {"status_codes": self.status_codes,
                "version": self.version}
