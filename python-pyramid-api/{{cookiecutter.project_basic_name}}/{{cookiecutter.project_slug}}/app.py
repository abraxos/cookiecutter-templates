from os import path
from pathlib import Path
from pprint import pformat
from threading import current_thread

from pyramid.config import Configurator
from pyramid.router import Router
from gunicorn.app.base import BaseApplication
from typeguard import typechecked

from {{ cookiecutter.project_slug }}.metrics import MultiprocessMetrics
from {{ cookiecutter.project_slug }}.utils.config import AppConfig
from {{ cookiecutter.project_slug }}.utils.log import log, uuid
from {{ cookiecutter.project_slug }}.resources.healthcheck import Healthcheck

HERE = Path(path.dirname(__file__))
PACKAGE = HERE.parent
OPENAPI_FILE = (PACKAGE / '{{ cookiecutter.project_slug }}' / 'openapi') / '{{ cookiecutter.project_slug }}.yaml'
NO_LOG_CONTENT_TYPES = {'audio/', 'image/', 'video/', 'application/zip'}


# TODO: add request UUID into every error response so that problematic requests may be traced to log entries
def hack_thread_name_tween_factory(handler, _):
    def hack_thread_name_tween(request):
        # Hack in the request ID inside the thread's name for logging purposes
        thread = current_thread()
        original = thread.name
        thread.name = f"{original}] [{request.uuid}"
        log.info(f'REQUEST: {request.method} {request.url} {request.content_type}\n'
                 f'\tBODY: {request.body}')
        try:
            response = handler(request)
            log.info(f'RESPONSE: {response.status_code} {response.content_type}')
            if response.content_type:
                if response.content_type.startswith('application/json'):
                    for line in pformat(response.json_body).split('\n'):
                        log.info(line)
                elif not any(response.content_type.startswith(t) for t in NO_LOG_CONTENT_TYPES):
                    log.info(response.body)
        finally:
            # Restore the thread's original name when done
            thread.name = original
        return response
    return hack_thread_name_tween


@typechecked
def create_app(config: AppConfig, debug=False) -> Router:
    """Prepare a Pyramid app."""
    with Configurator() as configurator:
        if debug:
            configurator.include("pyramid_openapi3")
            configurator.pyramid_openapi3_spec(OPENAPI_FILE)
            configurator.pyramid_openapi3_add_explorer()
            log.info(f"OpenAPI spec available at http://{config.bind}:{config.port}/docs/")
        metrics = MultiprocessMetrics().register(configurator)
        Healthcheck(config, metrics).register(configurator)
        configurator.add_request_method(uuid, reify=True)  # add uuid property to every request
        configurator.add_tween('{{ cookiecutter.project_slug }}.app.hack_thread_name_tween_factory')
        configurator.scan()

        return configurator.make_wsgi_app()


@typechecked
class StandaloneApplication(BaseApplication):  # pylint: disable=W0223
    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application  # pragma: no cover
