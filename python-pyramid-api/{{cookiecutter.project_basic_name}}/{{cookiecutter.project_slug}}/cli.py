from pathlib import Path
from typing import Optional, NoReturn
from dataclasses import asdict

from typeguard import typechecked
import click
from click_path import PathlibPath

from {{ cookiecutter.project_slug }}.utils.log import log, set_loglevel, DEBUG, INFO
from {{ cookiecutter.project_slug }}.utils.config import AppConfig
from {{ cookiecutter.project_slug }}.utils.fs import load_configuration_or_die
from {{ cookiecutter.project_slug }}.utils.utils import number_of_workers
from {{ cookiecutter.project_slug }}.app import StandaloneApplication, create_app


@typechecked
def load_config_and_merge_with_cli_params(config_path: Optional[Path] = None,
                                          port: Optional[int] = None) -> AppConfig:
    config = {} if config_path is None else asdict(
        load_configuration_or_die(config_path=config_path)
    )
    if port is not None:
        config['port'] = port
    app_config = AppConfig(**config)
    log.info(app_config)
    return app_config


@typechecked
def run_server(config: AppConfig, debug=False) -> NoReturn:
    application = create_app(config, debug=debug)
    options = {
        'bind': '%s:%s' % (config.bind, config.port),
        'workers': number_of_workers(),
    }
    StandaloneApplication(application, options).run()
    raise RuntimeError("{{ cookiecutter.project_title }} server terminated improperly")


@click.command()
@click.option('-c', '--config', 'config_file', type=PathlibPath(exists=True, file_okay=True,
                                                                dir_okay=False, readable=True,
                                                                resolve_path=True),
              default=Path.cwd() / 'config.yaml', help='The address of a configuration '
                                                       'file for the {{ cookiecutter.project_title }}. Defaults '
                                                       'to config.yaml in the current '
                                                       'directory. This is required unless'
                                                       ' all of the required options are '
                                                       'provided')
@click.option('-p', '--port', type=int, default=None, help='Port that the application '
                                                           'should be served on. Defaults '
                                                           'to 5000')
@click.option('--debug', is_flag=True, default=False, help='Enable debug features (also makes an '
                                                           'OpenAPI endpoint available at http:/'
                                                           '/localhost:<PORT>/apidocs/)')
@typechecked
def main(config_file: Optional[Path], port: Optional[int], debug: bool):
    set_loglevel(DEBUG if debug else INFO)
    config = load_config_and_merge_with_cli_params(config_file, port)
    run_server(config, debug)


if __name__ == "__main__":
    main()  # pylint: disable=E1120
