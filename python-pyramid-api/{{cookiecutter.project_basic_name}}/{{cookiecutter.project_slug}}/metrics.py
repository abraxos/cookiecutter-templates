from multiprocessing import Manager
from multiprocessing.managers import DictProxy  # type: ignore
from typing import Dict, Tuple, List, Optional
from collections import defaultdict

from pyramid.config import Configurator
from pyramid.events import NewResponse
from typeguard import typechecked

from {{ cookiecutter.project_slug }}.utils.types import StatusCode, Timestamp
from {{ cookiecutter.project_slug }}.utils.utils import now
from {{ cookiecutter.project_slug }}.utils.log import log


TimestampedStatusCodes = List[Tuple[Timestamp, StatusCode]]


class MultiprocessMetrics():
    @typechecked
    def __init__(self, ttl_seconds: Optional[int] = None):
        self.manager = Manager()
        self._shared_memory: DictProxy = self.manager.dict()
        self._shared_memory['status_codes'] = []
        self._status_codes_lock = self.manager.Lock()
        self.ttl_seconds = ttl_seconds or 3600  # keep one hour of metrics by default
        log.debug(f"Multiprocess Metrics initialized with a TTL of {self.ttl_seconds} seconds")

    @property
    def status_codes(self) -> TimestampedStatusCodes:
        return self._shared_memory['status_codes']

    @typechecked
    def _status_codes_since(self, seconds_since: int) -> TimestampedStatusCodes:
        cutoff = Timestamp(now() - seconds_since)
        return [(t, s) for t, s in self.status_codes if t > cutoff]

    @typechecked
    def register_status_code(self, status_code: StatusCode) -> None:
        with self._status_codes_lock:
            self._shared_memory['status_codes'] = self._status_codes_since(self.ttl_seconds) + [(now(), status_code)]

    @typechecked
    def status_code_summary(self, since_seconds: Optional[int] = None) -> Dict[StatusCode, int]:
        since_seconds = since_seconds or self.ttl_seconds
        summary: Dict[StatusCode, int] = defaultdict(int)
        for _, code in self._status_codes_since(since_seconds):
            summary[code] += 1
        return summary

    @typechecked
    def register(self, configurator: Configurator) -> 'MultiprocessMetrics':
        @typechecked
        def record_response_status(response: NewResponse):  # pylint: disable=W0612
            self.register_status_code(StatusCode(response.response.status_code))

        configurator.add_subscriber(record_response_status, NewResponse)
        return self
