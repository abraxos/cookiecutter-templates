# {{ cookiecutter.project_title }} Developers' Guide

## Running Basic Tests

## Vagrant & Integration Tests

The unit tests are nice and all, but they do not confirm that our application connects properly to a database, for example. As such, this project comes with a complete [Vagrant](https://www.vagrantup.com/docs/) configuration which allows developers to spin up a VM that is uniquely configured to make integration testing convenient. The VM includes, amongst other things, a PostgreSQL database for the API to provide access to and a copy of the API itself that gets automatically spun up. The following documentation explains how it should be used.

*It is critical to understand that this should not be used as a production deployment configuration. It has multiple insecure settings, such as passwords set to "hackme" for the sake of expediting development. If you use this configuration in production, you will get hacked, as the password suggests.*

### Initial Setup

First off, you have to [install Vagrant](https://www.vagrantup.com/docs/installation) for your specific OS, with the [libvirt provider](https://computingforgeeks.com/using-vagrant-with-libvirt-on-linux/). You will also need to [install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) which is used as the provisioning system for our VM.

You should now be able to connect to the VM using: `vagrant ssh` and get a shell.

### Running the VM

*It is strongly recommended that you familiarize yourself with the [basics of Vagrant](https://www.vagrantup.com/docs/cli).*

To bring up a version of the VM, simply execute: `vagrant up` from the repo directory. It might take a while the first time. All subsequent times the VM will retain state. If something goes wrong with the VM, simply destroy it with `vagrant destroy`, and then execute `vagrant up` again. If you just want to re-run the provisioning on the same VM (which is fine, because [the provisioning system is idempotent](https://admantium.medium.com/ansible-idempotent-playbooks-a89bb0e012c9)) simply execute: `vagrant up --provision` or `vagrant reload --provision` the latter of which restarts the VM.

### Common Functions

Certain functionality is extremely commonly used during development, and as such needs to be easy. This includes:

- You can connect to the PostgreSQL database in two key ways:
  - From inside the VM itself (after `vagrant ssh`) with: `psql -d {{ cookiecutter.db_name }}`
  - From outside the VM itself (assuming you have a postgres client installed on your host machine, on Debian-based distros this is the `postgresql-client-12` package) with: `psql -h 11.0.0.10 -U vagrant -d {{ cookiecutter.db_name }}`
