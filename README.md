# Eugene's Cookiecutter Templates

A repository for all of my [software project templates using cookiecutter](https://github.com/cookiecutter/cookiecutter). Not many now, but I expect this to grow.

## Templates

- `cookiecutter vagrant-ansible-learn` - This template is a general setup to create a vagrant VM provisioned by ansible with some basic information, applications, and settings. Particularly useful for learning a new technology and developing ansible roles that can then be used in other ansible configurations.
- `cookiecutter vagrant-ansible-postgres` - This template is a general setup to create a vagrant VM, provision by ansible with a postgres database and a directory structure for developing said database.
- `cookiecutter python-click-app` - A template for a python CLI application with Click for controlling the interface, pyyaml and pydantic for configuration, and pytest for testing.
- `cookiecutter python-module` - A template for a python library and pytest for testing.

## Dev Notes

While developing templates, the following approach greatly speeds up the process of creating/testing/deleting templated directories:

```bash
rm -rf stuff-api && \
printf 'Stuff API\n\n\nAn API for stuff\n\n\n\n\n' | cookiecutter ~/Development/cookiecutter-templates/python-pyramid-api && \
cd stuff-api && \
poetry install && \
poetry shell; cd -
```

## Helper Scripts

***DEPRICATED: Post-Hooks are better than scripts like this***

### `cookiecutter-pip-latest.py`

Given proper naming of pip package version variables (namely `<package>_version`), this script will automatically fetch the latest available version of the package and set that as the default value before executing cookiecutter. In order to work properly, the variables have to be declared in `cookiecutter.json` like so:

```json
{
    ...
    "pyyaml_version": "{{ cookiecutter.pyyaml_version }}",
    "click_version": "{{ cookiecutter.click_version }}"
}
```

and then wherever you want to use them, in this case `pyproject.toml`:

```toml
...
[tool.poetry.dependencies]
python = "^3.7"
click = "^{{ cookiecutter.click_version }}"
pyyaml = "^{{ cookiecutter.pyyaml_version }}"
...
```

The script itself is run like so:

```
cookiecutter-pip-latest.py <package-path>
```