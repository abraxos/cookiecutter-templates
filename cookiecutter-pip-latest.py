#!/usr/bin/env python3
"""
A script that automatically retrieves the latest version of any configured
Python packages, and then sets associated variables to that version.

In order to work properly, the variable must be named `<package>_version` and
the package in question needs to be accessible to Pip. The variables also have
to be declared in cookiecutter.json. Note that you must use ubderscores in place
of dashes for version variable names.

For example:
--------------------------------------------------------------------------------
cookiecutter.json:
--------------------------------------------------------------------------------
"pyyaml_version": "{{ cookiecutter.pyyaml_version }}",
"click_version": "{{ cookiecutter.click_version }}"
--------------------------------------------------------------------------------

and...

--------------------------------------------------------------------------------
pyproject.toml
--------------------------------------------------------------------------------
[tool.poetry.dependencies]
python = "^3.7"
click = "^{{ cookiecutter.click_version }}"
pyyaml = "^{{ cookiecutter.pyyaml_version }}"
--------------------------------------------------------------------------------
"""

from pathlib import Path
from pprint import pprint
from re import compile as regex
from subprocess import check_output, STDOUT
from sys import argv
from typing import Tuple, Set

from cookiecutter.main import cookiecutter

P_PAT = regex(r'{{\s*cookiecutter\.(?P<package>[0-9a-zA-Z_\.]+)_version\s*}}\"')
V_PAT = regex(r'[0-9a-zA-Z_\.]+\s\((?P<version>[\d\.]+)\)')

def latest_version(package: str) -> str:
    pip_output = check_output(['pip3', 'index', 'versions', package], stderr=STDOUT).decode()
    return V_PAT.findall(pip_output)[0]

package_path = Path(argv[1])

versioned: Set[Tuple[str, str]] = set()
for file_path in package_path.rglob('*'):
    if file_path.is_file():
        results = P_PAT.findall(file_path.open().read())
        if results:
            for result in results:
                versioned.add(result)

extra_context = {f'{p}_version': latest_version(p) for p in versioned}

cookiecutter(str(package_path), extra_context=extra_context)